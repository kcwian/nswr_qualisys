from launch import LaunchDescription 
from launch_ros.actions import Node
from ament_index_python.packages import get_package_share_directory
import os

def generate_launch_description():
        
    rviz_cfg_file = os.path.join(get_package_share_directory('nswr_qualisys'), 'nswr_qualisys.rviz')
    print(rviz_cfg_file)     
    
    return LaunchDescription([

        Node(
            package="nswr_qualisys",
            executable="nswr_qualisys",
            output="screen",
            parameters=[{'use_sim_time': False}]
        ),
        
        Node(
            package="rviz2",
            executable="rviz2",
            output="screen",
            arguments=["-d" + rviz_cfg_file],
            parameters=[{'use_sim_time': False}]
        ),    
         
    ])