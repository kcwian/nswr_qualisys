import rclpy
from rclpy.node import Node
from nav_msgs.msg import Odometry
from std_msgs.msg import Header
from sensor_msgs.msg import PointCloud2, PointField
from geometry_msgs.msg import Quaternion, TransformStamped
from tf2_ros import TransformException, TransformBroadcaster
from tf2_ros.buffer import Buffer
from tf2_ros.transform_listener import TransformListener
from tf_transformations import quaternion_matrix, quaternion_from_euler
import numpy as np
import math


class Qualisys(Node):

    def __init__(self):
        super().__init__('nswr_qualisys')

        self.tf_buffer = Buffer()
        self.tf_listener = TransformListener(self.tf_buffer, self)
        self.tf_broadcaster = TransformBroadcaster(self)
        self.timer = self.create_timer(0.1, self.node_callback)


    # Runs every 100 ms        
    def node_callback(self):

        # Read the "helmet" to "camera_left" transform (https://docs.ros.org/en/foxy/Tutorials/Intermediate/Tf2/Writing-A-Tf2-Listener-Py.html)
        try:
            # self.helmet2cameraLeftTs = ...

            # Convert self.helmet2cameraLeftTs transform to 4x4 homogeneous matrix
            # You can use quaternion_matrix() function to convert the quaternion to rotation matrix
            # (https://github.com/DLu/tf_transformations/blob/main/tf_transformations/__init__.py)
            helmet2cameraLeftMat = np.identity(4)
 
        except TransformException as ex:
            return

        # Define geometry_msgs::TransformStamped message that will be published in ROS (https://docs.ros2.org/foxy/api/geometry_msgs/msg/TransformStamped.html)
        cameraLeft2newSensorTs = TransformStamped()

        # Fill the timestamp of the cameraLeft2newSensorTs message
        cameraLeft2newSensorTs.header.stamp = self.get_clock().now().to_msg()

        # Fill the rest of the cameraLeft2newSensorTs message (header.frame_id, child_frame_id, and transform)



        # Publish the created transform
        # self.tf_broadcaster.sendTransform()
        
        # Create a new 4x4 Matrix containing transformation from "camera_left" to "new_sensor", based on the cameraLeft2newSensorTs variable
        # cameraLeft2newSensorMat = ...



        # Calculate the 4x4 Matrix that contains the transform from frame "helmet" to "new_sensor"
        # helmet2newSensorMat = ...
        


def main(args=None):

    rclpy.init(args=args)
    qualisys = Qualisys()

    rclpy.spin(qualisys)

    qualisys.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()